package com.stack.tech.enumtype;

public enum AddressTypeEnum {
    PERMANENT,
    TEMPORARY
}
