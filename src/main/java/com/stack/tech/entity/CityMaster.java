package com.stack.tech.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name="CITY_MASTER")
public class CityMaster {

	@Id
	@GeneratedValue(generator = "SEQ_CITY_MASTER")
	private Long id;
	private String name;
	private String countrycode;
	private String district;
	private Long population;

}
