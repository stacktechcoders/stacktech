package com.stack.tech.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "Manager_Master")
public class ManagerMaster {

    @Id
    @GeneratedValue(generator = "SEQ_MANAGER_MASTER")
    private Long employeeId;
    private String employeeName;
    private String departmentName;
    private String reportingName;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL ,mappedBy = "manager")
    private PanCard pancard;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL ,mappedBy = "manager")
    private List<AddressMaster> address;

    @ManyToMany
    private List<ProjectMaster> project;
}
