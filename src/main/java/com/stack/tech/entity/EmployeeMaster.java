package com.stack.tech.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name="Employee_Master")
public class EmployeeMaster {
	
	@Id
	@GeneratedValue(generator = "SEQ_EMPLOYEE_MASTER")
	private Long srNo;
	private String name;
	private String city;
	private Integer mobile;
	private Integer age;

}
