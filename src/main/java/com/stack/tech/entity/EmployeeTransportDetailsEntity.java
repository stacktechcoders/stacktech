package com.stack.tech.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Data
@Entity
@Table(name = "Employee_Transport_Details")
public class EmployeeTransportDetailsEntity {
    @Id
    @GeneratedValue(generator = "SEQ_TRANSPORT_DETAILS")
    private Long transportId;
    private String transportName;
    private String transportType;
    private String pickupLocation;
    private String dropLocation;
}
