package com.stack.tech.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="DEPARTMENT_MASTER")
public class DepartmentMaster {

    @Id
    @GeneratedValue(generator = "SEQ_DEPARTMENT_MASTER")
    private Long srNo;
}
