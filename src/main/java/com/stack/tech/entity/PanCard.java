package com.stack.tech.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Pan_Card")
public class PanCard {

    @Id
    @GeneratedValue(generator = "SEQ_PAN_CARD")
    private Long rowNo;
    private String name;
    private String panNo;

    @OneToOne
    @JoinColumn(name="Employee_ID")
    private ManagerMaster manager;
}
