package com.stack.tech.entity;

import com.stack.tech.enumtype.AddressTypeEnum;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="ADDRESS_MASTER")
public class AddressMaster {

    @Id
    @GeneratedValue(generator = "SEQ_ADDRESS_MASTER")
    private Long addressId;

    @Enumerated(EnumType.STRING)
    private AddressTypeEnum addressType;

    private String street1;
    private String landmark;
    private Long pincode;

    @ManyToOne
    @JoinColumn(name="Employee_ID")
    private ManagerMaster manager;
}
