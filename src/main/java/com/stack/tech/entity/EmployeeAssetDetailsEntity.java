package com.stack.tech.entity;


import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "Employee_Asset_Details")
public class EmployeeAssetDetailsEntity {

    @Id
    @GeneratedValue(generator = "SEQ_ASSET_DETAILS")
    private Long assetId;
    private String employeeName;
    private String department;
    private String handoverBy;
    private String particulars;
    private String remarks;

}
