package com.stack.tech.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "Project_Master")
public class ProjectMaster {

    @Id
    @GeneratedValue(generator = "SEQ_PROJECT_MASTER")
    private Long projectId;
    private String projectName;

    @ManyToMany
    private List<ManagerMaster> manager;
}
