package com.stack.tech.repository;

import com.stack.tech.entity.EmployeeAssetDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeAssetDetailsRepo extends JpaRepository<EmployeeAssetDetailsEntity,Long> {
}
