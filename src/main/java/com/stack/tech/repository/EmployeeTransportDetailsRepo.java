package com.stack.tech.repository;

import com.stack.tech.entity.EmployeeTransportDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeTransportDetailsRepo extends JpaRepository<EmployeeTransportDetailsEntity,Long> {


}
