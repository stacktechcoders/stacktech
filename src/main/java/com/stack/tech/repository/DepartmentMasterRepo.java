package com.stack.tech.repository;

import com.stack.tech.entity.DepartmentMaster;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentMasterRepo extends CrudRepository<DepartmentMaster,Long> {
}
