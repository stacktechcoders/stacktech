package com.stack.tech.repository;

import com.stack.tech.entity.AddressMaster;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressMasterRepository extends CrudRepository<AddressMaster,Long> {
}
