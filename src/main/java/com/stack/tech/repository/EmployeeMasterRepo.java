package com.stack.tech.repository;

import com.stack.tech.entity.EmployeeMaster;
import com.stack.tech.projection.EmployeeMasterProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeMasterRepo extends CrudRepository<EmployeeMaster, Long>, JpaRepository<EmployeeMaster, Long> {

    //----------------------------- JPA Methods------------------------------------------
    public List<EmployeeMaster> findByName(String name);

    //JPA Methods
    public EmployeeMaster findByNameAndSrNo(String name, Long srno);


    //-------------------------NAtive Query------------------------------------------------
    @Query(value = "Select * from employee_master", nativeQuery = true)
    public List<EmployeeMaster> puraListLekar();

    @Query(value = "Select * from employee_master where sr_no=?1", nativeQuery = true)
    public List<EmployeeMaster> getById(Long id);

    @Query(value = "Select * from employee_master where name=?1 and city=?2", nativeQuery = true)
    public List<EmployeeMaster> getByNameAndCity(String name, String city);

    @Query(value = "Select name,city,mobile from employee_master where city=?1", nativeQuery = true)
    public List<EmployeeMasterProjection> getByNativeProjection(String city);

    //----------------------------- Update data using @Modifying----------------------------------------
    @Modifying
    @Query(value = "update employee_master set name=?1  where sr_no=?2", nativeQuery = true)
    public void updateByNative(String name, Long srno);


    //---------------------HQL : HIbernate Query Lang--------------------------------------
    //  Note: Select *  do not work with HQL

    @Query(value = " from EmployeeMaster")
    public List<EmployeeMaster> getAllListByHQL();

    @Query(value = " from EmployeeMaster where srNo=:id")
    public List<EmployeeMaster> getByIdUsingHQL(Long id);

    @Query(value = " from EmployeeMaster where name=:n and city=:c")
    public List<EmployeeMaster> getByNameAndCityUsingHQL(String n, String c);

    @Query(value = "Select city from EmployeeMaster where city=:c")
    public List<String> getByCityUsingHQL(String c);

    @Query(value = "select name,city,mobile from EmployeeMaster where city=:c")
    public List<EmployeeMasterProjection> getByNameCityMobile(String c);


}
