package com.stack.tech.repository;

import com.stack.tech.entity.ManagerMaster;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerMasterRepo extends CrudRepository<ManagerMaster, Long> {
}
