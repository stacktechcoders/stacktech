package com.stack.tech.repository;

import com.stack.tech.entity.CityMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface CityRepository extends JpaRepository<CityMaster,Long> {

}
