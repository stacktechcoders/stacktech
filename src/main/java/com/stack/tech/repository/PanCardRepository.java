package com.stack.tech.repository;

import com.stack.tech.entity.PanCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PanCardRepository extends CrudRepository<PanCard,Long> {
}
