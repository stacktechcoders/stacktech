package com.stack.tech.repository;

import com.stack.tech.entity.ProjectMaster;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectMasterRepository extends CrudRepository<ProjectMaster,Long> {
}
