package com.stack.tech.common;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class ResponseObject {

	Map<String,Object> data = new HashMap<String,Object>();
	
	public void addData(String key , Object value) {
		data.put(key, value);
	}

	public Map<String, Object> getData() {
		return data;
	}

}
