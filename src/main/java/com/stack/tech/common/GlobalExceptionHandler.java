package com.stack.tech.common;

import com.stack.tech.common.exception.InValidUserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.BindException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice // = @ControllerAdvice + @ResponseBody........So you dont need @ResponseBody at each method level
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value=NullPointerException.class)
    public String nullPointerExceptionHandler() {
        log.error("NullPointerException is thrown from GlobalExceptionHandler");
        return "NullPointerException -> inside GlobalExceptionHandler message is appearing.";
    }

    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    @ExceptionHandler(value= InValidUserException.class)
    public String inValidUserExceptionHandler() {
        log.error("InValidUserException is thrown from GlobalExceptionHandler");
        return "InValidUserException -> inside GlobalExceptionHandler message is appearing.";
    }

    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value=Exception.class)
    public String genricExceptionHandler() {
        log.error("Exception is thrown from GlobalExceptionHandler");
        return "GlobalExceptionHandler -> Oops!!! Something went wrong.";
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        int i=0;
        Map<String, Object> body = new LinkedHashMap<>();
        ExceptionResponse exception = null ;
        for( i=0;i<ex.getBindingResult().getFieldErrors().size(); i++)
        {
            exception = new ExceptionResponse();
            exception.setDefaultMessage(ex.getBindingResult().getFieldErrors().get(i).getDefaultMessage());
            exception.setField(ex.getBindingResult().getFieldErrors().get(i).getField());
            body.put(ex.getBindingResult().getFieldErrors().get(i).getField(), exception);
        }
        body.put("ExceptionKey",ex.getBindingResult().getFieldErrors());
        return new ResponseEntity<Object>(body,status);
    }


}
