package com.stack.tech.common.exception;

public class InValidUserException extends RuntimeException{

    public InValidUserException(String str) {
        super(str);
    }
}
