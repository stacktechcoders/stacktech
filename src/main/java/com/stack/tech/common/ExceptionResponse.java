package com.stack.tech.common;

import lombok.Data;

@Data
public class ExceptionResponse {
    private String defaultMessage;
    private String field;
}
