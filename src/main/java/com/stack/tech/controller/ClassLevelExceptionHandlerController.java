package com.stack.tech.controller;

import com.stack.tech.service.ClassLevelExceptionHandlerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("exception")
public class ClassLevelExceptionHandlerController {

    private ClassLevelExceptionHandlerService classLevelExceptionHandlerService;

    public ClassLevelExceptionHandlerController(ClassLevelExceptionHandlerService classLevelExceptionHandlerService) {
        super();
        this.classLevelExceptionHandlerService = classLevelExceptionHandlerService;
    }

    //http://localhost:8085/stacktech/exception/nullException
    @GetMapping("nullException")
    public int getNullPointerException() {
        return classLevelExceptionHandlerService.getNullPointerException();
    }

    //http://localhost:8085/stacktech/exception/customCheckedException
    @GetMapping("customCheckedException")
    public int getCustomCheckedException() {
        log.info("CustomCheckedException API is called.");
        return classLevelExceptionHandlerService.getCustomCheckedException();
    }

    // Note: These 2 Methods has been moved to GlobalExceptionHandler class

//    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
//    @ExceptionHandler(value=NullPointerException.class)
//    public String nullPointerExceptionHandler() {
//        log.error("NullPointerException is thrown");
//        return "Null Pointer Exception !!!";
//    }
//
//    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR)
//    @ExceptionHandler(value=Exception.class)
//    public String genricExceptionHandler() {
//        log.error("Exception is thrown");
//        return "Genric Exception !!!";
//    }
}
