package com.stack.tech.controller;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.service.DepartmentMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MasterScreenController {

    @Autowired
    private DepartmentMasterService departmentMasterService;

    @GetMapping("getFindAll")
    public ResponseObject getFindAll(){
        return  departmentMasterService.getFindAll();
    }
}
