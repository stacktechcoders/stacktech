package com.stack.tech.controller;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.dto.EmployeeAssetDetailsDto;
import com.stack.tech.dto.EmployeeTransportDetailsDto;
import com.stack.tech.service.EmployeeAssetDetailsService;
import com.stack.tech.service.EmployeeTransportDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class TransportAssetController {

    @Autowired
    private EmployeeTransportDetailsService employeeTransportDetailsService;
    @Autowired
    private EmployeeAssetDetailsService  employeeAssetDetailsService;

    @PostMapping("insertEmpTransportData")
    public String insertEmpTransportData(@Valid @RequestBody EmployeeTransportDetailsDto employeeTransportDetailsDto){
       return employeeTransportDetailsService.insertEmpTransportData(employeeTransportDetailsDto);
    }

    @GetMapping("fetchAllEmpTransportData")
    public ResponseObject fetchAllEmpTransportData(){
        return  employeeTransportDetailsService.fetchAllEmpTransportData();
    }

    @PostMapping("fetchEmpTransportDataById/{transportId}")
    public ResponseObject fetchEmpTransportDataById(@PathVariable Long transportId){
        return  employeeTransportDetailsService.fetchEmpTransportDataById(transportId);
    }

   @PostMapping("updateEmpTransportData")
    public String updateEmpTransportData(@RequestBody EmployeeTransportDetailsDto employeeTransportDetailsDto){
        return  employeeTransportDetailsService.updateEmpTransportData(employeeTransportDetailsDto);
    }
    @PostMapping("deleteEmpTransportDataById/{transportId}")
    public String deleteEmpTransportDataById(@PathVariable Long transportId){
        return employeeTransportDetailsService.deleteEmpTransportDataById(transportId);
    }

    @PostMapping("insertEmpAssetData")
    public String insertEmpAssetData(@Valid @RequestBody EmployeeAssetDetailsDto employeeAssetDetailsDto){
        return employeeAssetDetailsService.insertEmpAssetData(employeeAssetDetailsDto);
    }

    @GetMapping("fetchAllEmpAssetData")
    public ResponseObject fetchAllEmpAssetData() {
        return employeeAssetDetailsService.fetchAllEmpAssetData();
    }

    @GetMapping("fetchEmpAssetById/{id}")
    public  ResponseObject fetchEmpAssetById(@PathVariable Long id){
    return employeeAssetDetailsService.fetchEmpAssetById(id);
    }
    @PostMapping("updateEmpAssetData")
    public String updateEmpAssetData(@RequestBody EmployeeAssetDetailsDto employeeAssetDetailsDto){
        return  employeeAssetDetailsService.updateEmpAssetData(employeeAssetDetailsDto);
    }
    @PostMapping("deleteEmpAssetDataById/{assetId}")
    public String deleteEmpAssetDataById(@PathVariable Long assetId){
        return employeeAssetDetailsService.deleteEmpAssetDataById(assetId);
    }
}
