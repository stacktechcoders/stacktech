package com.stack.tech.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value="this is swagger value", description="this is description")
@RestController
@RequestMapping("restApi")
public class SwaggerController {

    @ApiOperation(value="this is value", notes = "this is notes")
    @GetMapping("abc")
    public String abc(){
        return "abcd";
    }
}
