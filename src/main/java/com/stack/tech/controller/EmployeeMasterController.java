package com.stack.tech.controller;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.enumtype.ResponseTypeEnum;
import com.stack.tech.dto.EmployeeDto;
import com.stack.tech.service.EmployeeMasterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


@RestController
@Slf4j
@RequestMapping("employee")
public class EmployeeMasterController {

	@Autowired
	private EmployeeMasterService service;

	@GetMapping("getName")
	public String getName() {
		return "StackTech";
	}

	@GetMapping("getEmployee")
	public ResponseObject getEmployee() {
		return service.getEmployee();
	}

	@GetMapping("getAllList")
	public ResponseObject getAllList(){
		return service.getAllList();
	}

	@GetMapping("getallCity")
	public ResponseObject getallCity() {
		return service.getallCity();
	}

    @GetMapping("getNativeQuery")
    public ResponseObject getNativeQuery() {
        return service.getNativeQuery();
    }

    @GetMapping("getAllByHQL")
    public ResponseObject getAllByHQL(){
		return service.getAllMethodByHQL();
	}

	//http://localhost:8085/stacktech/employee/setRollNumber
	// Only pass name in body e.g Yuvraj , dont pass name:Yuvraj or name=Yuvraj etc.
	@PostMapping("setRollNumber")
	public ResponseTypeEnum setRollNumber(@RequestBody String name) {
		log.info("Providing Roll Number for name {}", name);
		return service.saveName(name);
	}

	@PostMapping("saveEmployee")
	public ResponseTypeEnum saveEmployee(@Valid @RequestBody EmployeeDto employeeDto) {
		log.info("EmployeeDto class values are: {}", employeeDto);
		return service.saveEmployee(employeeDto);
	}

	//http://localhost:8085/stacktech/employee/findById
	// Only pass ID number in body e.g 3 , dont pass id:3 or id=3 etc.
	@PostMapping("findById")
	public ResponseObject findById(@RequestBody Long id) {
		log.info("Find by ID using @RequestBody and ID: {}", id);
		return service.findById(id);
	}

	//http://localhost:8085/stacktech/employee/findByIdByRequestParam?id=3
	@PostMapping("findByIdByRequestParam")
	public ResponseObject findByIdByPathVariable(@RequestParam(value="id") long id) {
		log.info("Find by ID using @RequestParam and ID: {}", id);
		return service.findById(id);
	}

	//http://localhost:8085/stacktech/employee/findByIdByPathVariable/3
	@PostMapping("findByIdByPathVariable/{id}")
	public ResponseObject findByIdByPathVariable(@PathVariable Long id) {
		log.info("Find by ID using @PathVariable and ID: {}", id);
		return service.findById(id);
	}

	 //http://localhost:8085/stacktech/employee/findByName/Sameer
	 @PostMapping("findByName/{name}")
	 public ResponseObject findByName(@PathVariable String name) {
		 log.info("Find byName using @PathVariable and Name: {}", name);
		 return service.findByName(name);
	 }

	 //http://localhost:8085/stacktech/employee/updateByNative/Sameer/3
	@PostMapping("updateByNative/{name}/{srno}")
	public ResponseTypeEnum updateByNative(@PathVariable String name , @PathVariable Long srno) {
		log.info("Find by using @PathVariable and Name: {} & SrNo: {}", name, srno);
		return service.updateByNative(name,srno);
	}
}
