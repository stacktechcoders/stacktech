package com.stack.tech.controller;

import com.stack.tech.enumtype.ResponseTypeEnum;
import com.stack.tech.service.HibernateRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hibernate")
public class HibernateRelationshipController {

    @Autowired
    private HibernateRelationshipService hibernateRelationshipService;

    //http://localhost:8085/stacktech/hibernate/saveOneToOne
    @GetMapping("saveOneToOne")
    public ResponseTypeEnum saveOneToOne(){
        return hibernateRelationshipService.saveOneToOne();
    }

    //http://localhost:8085/stacktech/hibernate/saveOneToMany
    @GetMapping("saveOneToMany")
    public ResponseTypeEnum saveOneToMany(){
        return hibernateRelationshipService.saveOneToMany();
    }

    //http://localhost:8085/stacktech/hibernate/saveManyToMany
    @GetMapping("saveManyToMany")
    public ResponseTypeEnum saveManyToMany(){
        return hibernateRelationshipService.saveManyToMany();
    }

}
