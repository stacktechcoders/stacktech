package com.stack.tech.serviceImpl;

import com.stack.tech.common.exception.InValidUserException;
import com.stack.tech.service.ClassLevelExceptionHandlerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ClassLevelExceptionHandlerServiceImpl implements ClassLevelExceptionHandlerService {

    @Override
    public int getNullPointerException() {
        log.info("nullException API ...Service Impl is being called");
        String str = null;
        return str.length();
    }

    @Override
    public int getCustomCheckedException() {
        log.info("InValidUserException API ...Service Impl  is being called");
        throw new InValidUserException("In valid user data !!");
    }
}
