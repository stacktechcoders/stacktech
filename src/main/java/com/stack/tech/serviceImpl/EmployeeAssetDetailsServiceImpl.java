package com.stack.tech.serviceImpl;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.entity.EmployeeAssetDetailsEntity;
import com.stack.tech.dto.EmployeeAssetDetailsDto;
import com.stack.tech.repository.EmployeeAssetDetailsRepo;
import com.stack.tech.service.EmployeeAssetDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeAssetDetailsServiceImpl implements EmployeeAssetDetailsService {
    @Autowired
    private EmployeeAssetDetailsRepo employeeAssetDetailsRepo;

    @Autowired
    private ResponseObject response;

    @Override
    public String insertEmpAssetData(EmployeeAssetDetailsDto employeeAssetDetailsDto) {
        EmployeeAssetDetailsEntity employeeAssetDetailsEntity = new EmployeeAssetDetailsEntity();
        employeeAssetDetailsEntity.setDepartment(employeeAssetDetailsDto.getDepartment());
        employeeAssetDetailsEntity.setEmployeeName(employeeAssetDetailsDto.getEmployeeName());
        employeeAssetDetailsEntity.setParticulars(employeeAssetDetailsDto.getParticulars());
        employeeAssetDetailsEntity.setHandoverBy(employeeAssetDetailsDto.getHandoverBy());
        employeeAssetDetailsEntity.setRemarks(employeeAssetDetailsDto.getRemarks());
        employeeAssetDetailsRepo.save(employeeAssetDetailsEntity);

        return "Insert Emp Asset data sucessfully";

    }
    @Override
    public ResponseObject fetchAllEmpAssetData(){
        response.addData("fetchAllEmpAssetList", employeeAssetDetailsRepo.findAll());
       return  response;
    }

    @Override
    public ResponseObject fetchEmpAssetById(Long id) {
       response.addData("fetchEmpAssetById",employeeAssetDetailsRepo.findById(id));
        return response;
    }

    @Override
    public String updateEmpAssetData(EmployeeAssetDetailsDto employeeAssetDetailsDto) {
        if(employeeAssetDetailsDto.getAssetId() !=null && employeeAssetDetailsRepo.existsById(employeeAssetDetailsDto.getAssetId())) {
            EmployeeAssetDetailsEntity employeeAssetDetailsEntity = new EmployeeAssetDetailsEntity();
            employeeAssetDetailsEntity.setAssetId(employeeAssetDetailsDto.getAssetId());
            employeeAssetDetailsEntity.setEmployeeName(employeeAssetDetailsDto.getEmployeeName());
            employeeAssetDetailsEntity.setRemarks(employeeAssetDetailsDto.getRemarks());
            employeeAssetDetailsEntity.setHandoverBy(employeeAssetDetailsDto.getHandoverBy());
            employeeAssetDetailsEntity.setParticulars(employeeAssetDetailsDto.getParticulars());
            employeeAssetDetailsEntity.setDepartment(employeeAssetDetailsDto.getDepartment());
            employeeAssetDetailsRepo.save(employeeAssetDetailsEntity);
            return "Update Asset Details";
        }
            else {
                return " Asset id is null or No data found with respect to this Asset id";
            }
    }

    @Override
    public String deleteEmpAssetDataById(Long assetId) {
        employeeAssetDetailsRepo.deleteById(assetId);
        return "Delete Asset Details";
    }
}
