package com.stack.tech.serviceImpl;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.repository.DepartmentMasterRepo;
import com.stack.tech.service.DepartmentMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentMasterServiceImpl  implements DepartmentMasterService {

    @Autowired
    private DepartmentMasterRepo departmentMasterRepo;
    @Autowired
    private ResponseObject response;

    @Override
    public ResponseObject getFindAll() {
        response.addData("key",departmentMasterRepo.findAll());
        return response;
    }
}
