package com.stack.tech.serviceImpl;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.entity.EmployeeTransportDetailsEntity;
import com.stack.tech.dto.EmployeeTransportDetailsDto;
import com.stack.tech.repository.EmployeeTransportDetailsRepo;
import com.stack.tech.service.EmployeeTransportDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeTransportDetailsServiceImpl implements EmployeeTransportDetailsService {
    @Autowired
    private EmployeeTransportDetailsRepo employeeTransportDetailsRepo;
    @Autowired
    private ResponseObject response;

    @Override
    public String insertEmpTransportData(EmployeeTransportDetailsDto employeeTransportDetailsDto) {
        EmployeeTransportDetailsEntity transportDetailsDto = new EmployeeTransportDetailsEntity();
        transportDetailsDto.setTransportType(employeeTransportDetailsDto.getTransportType());
        transportDetailsDto.setTransportName(employeeTransportDetailsDto.getTransportName());
        transportDetailsDto.setDropLocation(employeeTransportDetailsDto.getDropLocation());
        transportDetailsDto.setPickupLocation(employeeTransportDetailsDto.getPickupLocation());
        employeeTransportDetailsRepo.save(transportDetailsDto);
        return "Insert Employee Transport Data Successfully";
    }

    @Override
    public ResponseObject fetchAllEmpTransportData() {
        response.addData("fetchAllEmpTransportList", employeeTransportDetailsRepo.findAll());
        return  response;
    }

    @Override
    public ResponseObject fetchEmpTransportDataById(Long transportId) {
        response.addData("fetchEmpTransportById",employeeTransportDetailsRepo.findById(transportId));
        return response;
    }

    @Override
    public String updateEmpTransportData(EmployeeTransportDetailsDto employeeTransportDetailsDto) {
        if(employeeTransportDetailsDto.getTransportId() !=null && employeeTransportDetailsRepo.existsById(employeeTransportDetailsDto.getTransportId())) {
            EmployeeTransportDetailsEntity transportDetailsDto = employeeTransportDetailsRepo.getOne(employeeTransportDetailsDto.getTransportId() );
            transportDetailsDto.setTransportType(employeeTransportDetailsDto.getTransportType());
            transportDetailsDto.setTransportName(employeeTransportDetailsDto.getTransportName());
            transportDetailsDto.setDropLocation(employeeTransportDetailsDto.getDropLocation());
            transportDetailsDto.setPickupLocation(employeeTransportDetailsDto.getPickupLocation());
            employeeTransportDetailsRepo.save(transportDetailsDto);
            return "Record Updated Success";
        }
        else {
            return " Transport id is null or No data found with respect to this Transport id";
        }
    }
    @Override
    public String deleteEmpTransportDataById(Long transportID) {
        employeeTransportDetailsRepo.deleteById(transportID);
        return "Update Success";
    }
}
