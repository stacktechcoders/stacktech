package com.stack.tech.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;

@Data
public class EmployeeAssetDetailsDto {
    @NotEmpty(message = "Employee Name is Required")
    private String employeeName;
    @NotEmpty(message = "Department Name is Required")
    private String department;
    @NotEmpty(message = "HandoverBy Name is Required")
    private String handoverBy;
    @NotEmpty(message = "Transport Name is Required")
    private String particulars;
    private String remarks;
    private Long assetId;
}
