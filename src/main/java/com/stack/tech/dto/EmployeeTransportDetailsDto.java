package com.stack.tech.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class EmployeeTransportDetailsDto {
    @NotEmpty(message = "Transport Name Is Required")
    @Size(max = 50,message = "Transport Name is Should be 50 maximum character only")
    private String transportName;
    @NotEmpty(message = "Transport Type  is Required")
    private String transportType;
    @NotEmpty(message = "Pickup Location  is Required")
    private String pickupLocation;
    @NotEmpty(message = "Drop Location  is Required")
    private String dropLocation;
    private Long transportId;
}
