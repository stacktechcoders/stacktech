package com.stack.tech.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtRequestDto implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    private String username;
    private String password;

    //need default constructor for JSON Parsing
    public JwtRequestDto()
    {
      super();
    }

    public JwtRequestDto(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

}
