package com.stack.tech.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
public class EmployeeDto {

	@NotEmpty(message = "Please provide a name")
	@Size(max=5 , message = "Name characters should be 5 Characters only")
	private String name;

//	@Max(150)
	@Digits(integer=2 , fraction = 0)
	private Integer age;

	@Digits(integer=10 , fraction = 0)
//	@Max(value= 999999999 , message = "Mobile no should be 10 digits only")
	private Integer mobileno;

	@NotEmpty(message = "Please provide a City")
	private String city;

}
