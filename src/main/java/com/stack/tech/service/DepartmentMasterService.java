package com.stack.tech.service;

import com.stack.tech.common.ResponseObject;

public interface DepartmentMasterService {

    public ResponseObject getFindAll();
}
