package com.stack.tech.service;

import com.stack.tech.enumtype.AddressTypeEnum;
import com.stack.tech.enumtype.ResponseTypeEnum;
import com.stack.tech.entity.AddressMaster;
import com.stack.tech.entity.ManagerMaster;
import com.stack.tech.entity.PanCard;
import com.stack.tech.entity.ProjectMaster;
import com.stack.tech.repository.AddressMasterRepository;
import com.stack.tech.repository.ManagerMasterRepo;
import com.stack.tech.repository.PanCardRepository;
import com.stack.tech.repository.ProjectMasterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class HibernateRelationshipService {

    @Autowired
    private ManagerMasterRepo managerMasterRepo;
    @Autowired
    private PanCardRepository panCardRepository;
    @Autowired
    private AddressMasterRepository addressMasterRepository;
    @Autowired
    private ProjectMasterRepository projectMasterRepository;

    @Transactional
    public ResponseTypeEnum saveOneToOne(){

        ManagerMaster manager = new ManagerMaster();
        manager.setEmployeeName("Swapnil");
        manager.setDepartmentName("TIB");
        manager.setReportingName("Pollard");

        PanCard pan = new PanCard();
        pan.setName("Swapnil");
        pan.setPanNo("ASDF4561K");

        manager.setPancard(pan);
        pan.setManager(manager);

        managerMasterRepo.save(manager);
        panCardRepository.save(pan);

        return ResponseTypeEnum.SUCCESS;
    }

    @Transactional
    public ResponseTypeEnum saveOneToMany(){

        ManagerMaster manager = new ManagerMaster();
        manager.setEmployeeName("Waqas");
        manager.setDepartmentName("KGH");
        manager.setReportingName("Pollard");

        AddressMaster dto1 = new AddressMaster();
        dto1.setAddressType(AddressTypeEnum.PERMANENT);
        dto1.setLandmark("JVLR");
        dto1.setStreet1("Service Road");
        dto1.setPincode(456123L);

        AddressMaster dto2 = new AddressMaster();
        dto2.setAddressType(AddressTypeEnum.TEMPORARY);
        dto2.setLandmark("Mumbai Central ");
        dto2.setStreet1("S.V Road");
        dto2.setPincode(456123L);

           List<AddressMaster> addressList = new ArrayList<>();
           addressList.add(dto1);
           addressList.add(dto2);

        manager.setAddress(addressList);  //manger Master Object Ready

        dto1.setManager(manager);
        dto2.setManager(manager);


        managerMasterRepo.save(manager);
        addressMasterRepository.save(dto1);
        addressMasterRepository.save(dto2);


        return ResponseTypeEnum.SUCCESS;
    }


    @Transactional
    public ResponseTypeEnum saveManyToMany(){

        ManagerMaster manager1 = new ManagerMaster();
        manager1.setEmployeeName("Waqas");
        manager1.setDepartmentName("KGH");
        manager1.setReportingName("Pollard");

        ManagerMaster manager2 = new ManagerMaster();
        manager2.setEmployeeName("Manoj");
        manager2.setDepartmentName("KLJ");
        manager2.setReportingName("Sachin");

        ProjectMaster project1 = new ProjectMaster();
        project1.setProjectName("Project 1");

        ProjectMaster project2 = new ProjectMaster();
        project2.setProjectName("Project 2");

        List<ManagerMaster> managerList = new ArrayList<>();
        managerList.add(manager1);
        managerList.add(manager2);

        List<ProjectMaster> projectList =  new ArrayList<>();
        projectList.add(project1);
        projectList.add(project2);

        manager1.setProject(projectList);
        manager2.setProject(projectList);

        project1.setManager(managerList);
        project2.setManager(managerList);

        managerMasterRepo.saveAll(managerList);
        projectMasterRepository.saveAll(projectList);

        return  ResponseTypeEnum.SUCCESS;

    }

}
