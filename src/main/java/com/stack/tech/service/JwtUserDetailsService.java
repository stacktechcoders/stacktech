package com.stack.tech.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    /*  POST http://localhost:8085/stacktech/authenticate
        {
            "username":"stacktechsecret",
            "password":"Asdf@1234"
        }

      Password has been provided in Bcrypt , use below URL to convert password in Bcrypt
      https://www.javainuse.com/onlineBcrypt
    */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if ("stacktechsecret".equals(username)) {
            return new User("stacktechsecret", "$2a$10$gaEWP9IcBH6shtzWFBYFvOGFNd9drE6gB3SLkvh80PM05RGf.ppEy",
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}
