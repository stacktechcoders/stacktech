package com.stack.tech.service;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.dto.EmployeeAssetDetailsDto;

public interface EmployeeAssetDetailsService {

    public String insertEmpAssetData(EmployeeAssetDetailsDto employeeAssetDetailsDto);

    public ResponseObject fetchAllEmpAssetData();

    public ResponseObject fetchEmpAssetById(Long id);

    public String updateEmpAssetData(EmployeeAssetDetailsDto employeeAssetDetailsDto);

    public String deleteEmpAssetDataById(Long assetId);

}
