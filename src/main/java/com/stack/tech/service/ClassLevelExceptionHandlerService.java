package com.stack.tech.service;

public interface ClassLevelExceptionHandlerService {

    int getNullPointerException();
    int getCustomCheckedException();
}
