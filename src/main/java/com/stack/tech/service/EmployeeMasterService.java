package com.stack.tech.service;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.enumtype.ResponseTypeEnum;
import com.stack.tech.entity.EmployeeMaster;
import com.stack.tech.dto.EmployeeDto;
import com.stack.tech.repository.CityRepository;
import com.stack.tech.repository.EmployeeMasterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeMasterService {

	@Autowired
	private CityRepository repo;
	@Autowired
	private EmployeeMasterRepo employeeMasterRepo;

	public ResponseObject getEmployee() {
		ResponseObject response = this.getNewResponse();
		response.addData("FistEmp", new EmployeeDto("Stack",30,789456132,"Mumbai"));
		return response;
	}

	public ResponseObject getAllList(){
		ResponseObject response = this.getNewResponse();
		List<EmployeeDto> list = new ArrayList<>();
		list.add(new EmployeeDto("Apple",30,789456132,"Mumbai"));
		list.add(new EmployeeDto("ball",30,789456132,"Mumbai"));
		list.add(new EmployeeDto("cat",30,789456132,"Mumbai"));
		list.add(new EmployeeDto("dog",30,789456132,"Mumbai"));
		response.addData("allEmpList",list);
		return response;

	}

	public ResponseObject getallCity() {
		ResponseObject response = this.getNewResponse();
		response.addData("cityList", repo.findAll());
		return response;
	}

	@Transactional
	public ResponseTypeEnum saveName(String name) {
		EmployeeMaster dto = new EmployeeMaster();
		dto.setName(name);
		employeeMasterRepo.save(dto);
		return ResponseTypeEnum.SUCCESS;
	}

	@Transactional
    public ResponseTypeEnum saveEmployee(EmployeeDto employeeDto) {
    	EmployeeMaster dto = new EmployeeMaster();
    	dto.setName(employeeDto.getName());
    	dto.setAge(employeeDto.getAge());
    	dto.setCity(employeeDto.getCity());
    	dto.setMobile(employeeDto.getMobileno());
    	employeeMasterRepo.save(dto);
    	return ResponseTypeEnum.SUCCESS;
	}

	@Transactional
    public ResponseObject findById(Long id) {
		ResponseObject response = this.getNewResponse();
    	response.addData("result", employeeMasterRepo.findById(id));
    	return response;
	}

	@Transactional
    public ResponseObject findByName(String name) {
		ResponseObject response = this.getNewResponse();
    	response.addData("list",employeeMasterRepo.findByName(name));
    	response.addData("nameAndSrno", employeeMasterRepo.findByNameAndSrNo(name,112L));
    	return response;
    }

	@Transactional
    public ResponseObject getNativeQuery(){
		ResponseObject response = this.getNewResponse();
		response.addData("allMazdoor",employeeMasterRepo.puraListLekar());
		response.addData("gettingById",employeeMasterRepo.getById(1L));
		response.addData("NameAndCity",employeeMasterRepo.getByNameAndCity("Stack","Mumbai"));
		response.addData("NativeProjection",employeeMasterRepo.getByNativeProjection("Mumbai"));
		return  response;
	}

	@Transactional
	public ResponseObject getAllMethodByHQL(){
		ResponseObject response = this.getNewResponse();
		response.addData("AllByHQL",employeeMasterRepo.getAllListByHQL());
		response.addData("GetById",employeeMasterRepo.getByIdUsingHQL(1L));
		response.addData("NameAndCity",employeeMasterRepo.getByNameAndCityUsingHQL("Stack","Mumbai"));
		response.addData("CityList",employeeMasterRepo.getByCityUsingHQL("Mumbai"));
		response.addData("NameCityMobile",employeeMasterRepo.getByNameCityMobile("Mumbai"));
		return  response;
	}

	@Transactional
	public ResponseTypeEnum updateByNative(String name , Long srno){
		employeeMasterRepo.updateByNative(name,srno);
		return ResponseTypeEnum.SUCCESS;
	}

	private ResponseObject getNewResponse() {
		return new ResponseObject();
	}
}
