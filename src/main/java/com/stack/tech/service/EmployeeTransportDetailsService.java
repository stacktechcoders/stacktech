package com.stack.tech.service;

import com.stack.tech.common.ResponseObject;
import com.stack.tech.dto.EmployeeTransportDetailsDto;

public interface EmployeeTransportDetailsService {

    public String insertEmpTransportData(EmployeeTransportDetailsDto employeeTransportDetailsDto);

    public ResponseObject fetchAllEmpTransportData();

    public ResponseObject fetchEmpTransportDataById(Long transportId);

    public String updateEmpTransportData(EmployeeTransportDetailsDto employeeTransportDetailsDto);

    public String deleteEmpTransportDataById(Long transportID);
}
