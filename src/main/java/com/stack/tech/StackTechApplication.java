package com.stack.tech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StackTechApplication {

	public static void main(String[] args) {
		SpringApplication.run(StackTechApplication.class, args);
	}

}
