package com.stack.tech.projection;

public interface EmployeeMasterProjection {

    public String getName();
    public String getCity();
    public Long getMobile();
}
