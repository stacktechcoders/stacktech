package com.stack.tech.interview.qualifier;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("car")
public class TataSafariVehicleServiceImpl implements VehicleService{

    @Override
    public String getEngineDetails() {
        return "Tata Safari diesel engine is 1956 cc";
    }
}
