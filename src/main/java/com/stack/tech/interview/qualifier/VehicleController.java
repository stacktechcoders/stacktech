package com.stack.tech.interview.qualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("vehicle")
public class VehicleController {
    //    ========================@Qualifier==========================
    /* NoUniqueBeanDefinitionException
     * This can be achieved 3 ways   1. @Primary    2. @Qualifier   3. ByName
     * */

//  2. @Qualifier
    @Autowired
    @Qualifier("car")
    private VehicleService vehicleServiceCar;

    @GetMapping("getEngineDetailsUsingQualifierCar")
    public String getEngineDetailsUsingQualifierCar() {
        return vehicleServiceCar.getEngineDetails();
    }


    @Autowired
    @Qualifier("bike")
    private VehicleService vehicleServiceBike;

    @GetMapping("getEngineDetailsUsingQualifierBike")
    public String getEngineDetailsUsingQualifierBike() {
        return vehicleServiceBike.getEngineDetails();
    }
//  ===================================================================================================================

//  3. By Name
    @Autowired
    private VehicleService tataSafariVehicleServiceImpl;
    @Autowired
    private VehicleService royalEnfieldBulletVehicleServiceImpl;

    @GetMapping("getEngineDetailsByName")
    public String getEngineDetailsByName() {
        return tataSafariVehicleServiceImpl.getEngineDetails() + " <=> "+royalEnfieldBulletVehicleServiceImpl.getEngineDetails();
    }
}
