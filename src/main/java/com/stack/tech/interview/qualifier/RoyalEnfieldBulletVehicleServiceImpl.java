package com.stack.tech.interview.qualifier;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Qualifier("bike")
public class RoyalEnfieldBulletVehicleServiceImpl implements VehicleService{

    @Override
    public String getEngineDetails() {
        return "Royal Enfield Bullet engine 350 cc";
    }
}
