package com.stack.tech.interview.qualifier;

public interface VehicleService {

    String getEngineDetails();
}
