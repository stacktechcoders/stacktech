package com.stack.tech.interview.beanConfiguration;

import com.stack.tech.dto.EmployeeDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class BeanService {

    public List<EmployeeDto> getBeanByType() {
        log.info("Get Bean By Type service is called :) ");
        EmployeeService employeeService = getApplicationContextScanAndRefresh().getBean(EmployeeService.class);
        return employeeService.getBeanList();
    }

    public List<EmployeeDto> getBeanByName() {
        log.info("Get Bean By Name service is called :) ");
        EmployeeService employeeService1  = (EmployeeService) getApplicationContextScanAndRefresh().getBean("EmployeeServiceBean1");
//        EmployeeService employeeService2  = (EmployeeService) getApplicationContextScanAndRefresh().getBean("EmployeeServiceBean2");
        return employeeService1.getBeanList();
    }

    private AnnotationConfigApplicationContext getApplicationContextScanAndRefresh() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.scan("com.stack.tech.interview.beanConfiguration");
        applicationContext.refresh();
        return applicationContext;
    }
}