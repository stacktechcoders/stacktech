package com.stack.tech.interview.beanConfiguration;

import com.stack.tech.dto.EmployeeDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("beanConfig")
@Slf4j
public class BeanController {

    private BeanService beanService;
    public BeanController(BeanService beanService) {
        this.beanService = beanService;
    }

    //http://localhost:8085/stacktech/beanConfig/getBeanByType
    @GetMapping("getBeanByType")
    public List<EmployeeDto> getBeanByType() {
        log.info("Get Bean By Type REST API is called :) ");
        return beanService.getBeanByType();
    }

    //http://localhost:8085/stacktech/beanConfig/getBeanByName
    @GetMapping("getBeanByName")
    public List<EmployeeDto> getBeanByName() {
        log.info("Get Bean By Name REST API is called :) ");
        return beanService.getBeanByName();
    }
}