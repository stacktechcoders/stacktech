package com.stack.tech.interview.beanConfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean(name= {"EmployeeServiceBean1","EmployeeServiceBean2"})
    public EmployeeService getEmployeeService() {
        return new EmployeeService();
    }
}
