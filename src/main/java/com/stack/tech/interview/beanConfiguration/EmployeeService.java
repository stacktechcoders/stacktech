package com.stack.tech.interview.beanConfiguration;

import com.stack.tech.dto.EmployeeDto;

import java.util.ArrayList;
import java.util.List;

public class EmployeeService {

    private static final List<EmployeeDto> list = new ArrayList<>();

    public List<EmployeeDto> getBeanList() {
        list.add(new EmployeeDto("Apple",30,789456132,"Mumbai"));
        list.add(new EmployeeDto("ball",30,789456132,"Mumbai"));
        return list;
    }

    public static List<EmployeeDto> getList() {
        return list;
    }
}